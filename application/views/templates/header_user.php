<style>
	* {
		box-sizing: border-box;
	}

	button {
		text-decoration: none;
	}

	#header {

		height: 10%;

	}

	#user {
		border-left: 2px groove;
		width: 100%;
		height: 100%;

	}

	#col2 {
		width: 100%;
		height: 100%;
	}

	#f {
		width: 60%;
		height: 100%;
	}

	#foto {

		width: 100%;
		height: auto;
		border-radius: 50%;
	}

	#foto2 {
		height: auto;
		width: 50%;
		border-radius: 50%;
	}

	#u {

		width: 70%;

	}

	#u2 {

		width: 100%;

	}

	#u2 span {
		width: 100%;
		font-weight: bold;
	}

	#card {
		width: 100%;
		height: 100%;
	}

	#card_user {
		border-radius: 12px;
	}

	#content {
		width: 40%;
		height: 100%;
	}

	#boton_user {
		width: 100%;
		height: 100%;
	}

	#row_foto {
		width: 100%;
		height: 100%;
	}

	#col_foto {
		width: 50%;
		height: auto;
	}

	#col_name {
		width: 50%;
		height: 100%;
	}

	#u {
		width: 100%;
		height: 100%;
	}

	/* p {
		height: 100%;
		width: 120px;
		padding: 12px;
		font-weight: bold;

	} */
	/* #boton_user {
		width: 400px;
		height: 100%;
	} */

</style>
<div id="header"
	class="container-fluid p-0 border border-top-0 border-right-0 border-left-0 d-flex justify-content-end">
	<div id="user" class="row m-0">

		<div class="col-sm-8">
		</div>

		<div class="col-sm-4 p-0" id="col2">

			<div class="dropdown d-flex justify-content-end align-items-center p-2" id="card">
				<button id="content" type="button" class="btn bg-primary " data-toggle="dropdown">

					<a id="boton_user" style="text-decoration:none" href="">
						<div id="row_foto" class="row">
							<div id="col_foto" class="col-sm-6">
								<div id="f" class=" d-flex justify-content-center align-items-center col-*-*"><img
										id="foto" class="" src="<?php echo base_url() ?>/public/img/foto.jpg" alt="">
								</div>
							</div>
							<div id="col_name" class="col-sm-6">
								<div id="u" class=" d-flex justify-content-center align-items-center col-*-*">
									<span 
										class="d-flex justify-content-center align-items-center text-white"><?php echo $nombre . " " . $apellido; ?></span>
								</div>
							</div>
						</div>

					</a>


				</button>
				<div class="dropdown-menu" id="card_user">
					<div class="row">
						<div class="col-sm-12">
							<div id="f2" class=" d-flex justify-content-center align-items-center m-2"><img id="foto2"
									class="" src="<?php echo base_url() ?>/public/img/foto.jpg" alt=""></div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 d-flex justify-content-center align-items-center">
							<div id="u2"
								class="container-fluid d-flex justify-content-center align-items-center border bg-primary text-white">
								<span
									class="mb-0 d-flex justify-content-center align-items-center"><?php echo $nombre . " " . $apellido; ?></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 d-flex justify-content-center align-items-center">
							<div id="u3" class="container-fluid d-flex justify-content-center align-items-center ">
								<span
									class="mb-0 d-flex justify-content-center align-items-center"><?php echo $puesto; ?></span>
							</div>
						</div>
					</div>
					<div class="row ">
						<div class="col-sm-12 d-flex justify-content-center align-items-center">
							<div id="boton_cerrar" class="container-fluid-sm p-3 d-flex justify-content-end"><a
									type="button" class="btn btn-primary bg-primary text-white"
									href="<?php echo base_url() ?>Login/cerrar_sesion">Cerrar Sesión</a></div>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>

<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

Class Listar extends CI_Controllers
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Listar_model');
	}

	public function listar_u()
	{
		$data = array();
		$data['lista'] = $this->Lista_Model->mostrar_usuarios();
	}
}

?>

<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="<?php echo base_url();?>public/css/lightbox.css" rel="stylesheet" />

	<link href="<?php echo base_url();?>public/css/bootstrap.css" rel="stylesheet" />

	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
	</script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
	</script>
	<title>Document</title>
</head>

<body>
	
	<script src="<?php echo base_url();?>public/js/lightbox-plus-jquery.js">
	</script>
	<script>
		lightbox.option({
			'resizeDuration': 200,
			'wrapAround': true,
			'positionFromTop': 200,
			'fadeDuration': 400,
			'albumLabel': ""
		})

	</script>
</body>

</html>

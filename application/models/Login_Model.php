
<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function usuario_nombre($nombre, $pass)
    {
        $this->db->select('usuario,contraseña, nombres, apellidos,puesto');
        $this->db->from('users');
        $this->db->where('usuario', $nombre);
        $this->db->where('contraseña', $pass);
        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }

    public function registrar_usuario()
    {
        $data = array(
            'usuario' => $this->input->post('usuarioR'),
            'contraseña' => $this->input->post('passR'),
            'nombres' => $this->input->post('nombreR'),
            'apellidos' => $this->input->post('apellidoR'),
            'correo' => $this->input->post('correoR'),
        );
        return $this->db->insert('users', $data);
        redirect(base_url() . 'user');

    }

    public function mostrar_usuarios()
    {
        $this->db->select('id,usuario,contraseña,nombres,apellidos,correo,puesto');
        $this->db->from('users');
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

	public function eliminar_usuarios($id)
	{
		$this->db->delete();
		$this->db->from('users');
		$this->db->where('');
	}
}

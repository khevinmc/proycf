<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="<?php echo base_url();?>public/css/lightbox.css" rel="stylesheet" />

	<link href="<?php echo base_url();?>public/css/bootstrap.css" rel="stylesheet" />


	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
	</script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
	</script>
	<title>Document</title>
	<style>
		a {
			border-radius: 8px;
		}

	</style>
</head>

<body>

	<section>
		<div class="container d-flex justify-content-center border mt-3 mb-3">
			<h2>Usuarios registrados en el sistema</h2>
		</div>
		<div class="container">
			<div class="table-responsive">

				<table class="table table-dark table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Usuario</th>
							<th>Contraseña</th>
							<th>Nombres</th>
							<th>Apellidos</th>
							<th>Correo</th>
							<th>Puesto</th>
							<th>EDITAR</th>
							<th>ELIMINAR</th>

						</tr>
					</thead>
					<tbody>
						<?php foreach ($lista as $item):?>
						<tr>
							<td><?php echo $item->id ?></td>
							<td><?php echo $item->usuario ?></td>
							<td><?php echo $item->contraseña ?></td>
							<td><?php echo $item->nombres ?></td>
							<td><?php echo $item->apellidos ?></td>
							<td><?php echo $item->correo ?></td>
							<td><?php echo $item->puesto ?></td>
							<td><a href="#" style="text-decoration:none" type="submit" class="bg-info p-2 text-white" data-toggle="modal" data-target="#myModal">Editar</a></td>
							<td><a href="#" style="text-decoration:none" type="submit" class="bg-primary p-2 text-white">Eliminar</a></td>


						</tr>

	<div class="modal fade" id="myModal">
		<div class="modal-dialog">
			<div class="modal-content" id="modalcss">

				<!-- Modal Header -->
				<div id="modalh" class="modal-header d-flex justify-content-center">
					<h4 class="modal-title ">Registro de Usuario</h4>

				</div>

				<!-- Modal body -->
				<div class="container p-3 d-flex justify-content-center border">
		<p>Usuario:</p><input type="text">
		<p>Contraseña</p><input type="text">
	</div>

			</div>
		</div>
	</div>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>


	</section>

	<script src="<?php echo base_url();?>public/js/lightbox-plus-jquery.js">
	</script>
	<script>
		lightbox.option({
			'resizeDuration': 200,
			'wrapAround': true,
			'positionFromTop': 50,
			'fadeDuration': 400,
			'albumLabel': ""
		})

	</script>
</body>

</html>

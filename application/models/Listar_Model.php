<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

Class Listar_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function mostrar_usuarios()
    {
        $this->db->select('id,usuario,contraseña,nombres,apellidos,correo,puesto');
        $this->db->from('users');
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }
}

?>

<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->library('session');
    }
    public function iniciar_sesion()
    {

        $data = array();
        $data['error'] = $this->session->flashdata('error');
        $this->load->view('templates/header');
        $this->load->view('login_view', $data);

        $this->load->view('templates/footer');
    }
    public function iniciar_session_post()
    {
        if ($this->input->post()) {
            $nombre = $this->input->post('usuario');
            $pass = $this->input->post('pass');
            // var_dump($this->input->post());exit;
            $this->load->model('Login_Model');

            $usuario1 = $this->Login_Model->usuario_nombre($nombre, $pass);
            // if($nombre ==  $usuario1 ->usuario || $pass == $usuario->contraseña){

            // }
            if ($usuario1) {
                $usuario_data = array(
					'id'=> $usuario1->id,
                    'usuario' => $nombre,
                    'contraseña' => $pass,
                    'nombre' => $usuario1->nombres,
                    'apellidos' => $usuario1->apellidos,
                    'puesto' => $usuario1->puesto,
                    'logueado' => true,
                );
                $this->session->set_userdata($usuario_data);
                // var_dump($this->session->userdata());exit;
                redirect(base_url() . 'user');
            } else {
                $this->session->set_flashdata('error', 'El usuario o la contraseña son incorrectos.');

                redirect(base_url());
            }

        } else {

            $this->iniciar_sesion();
        }

	}
	public function listar_u()
	{
		$data = array();
		$data['lista'] = $this->Lista_Model->mostrar_usuarios();
	}
    public function logueado()
    {
        if ($this->session->userdata('logueado')) {
            $user = $this->session->userdata('usuario');
            // var_dump( $this->session->userdata());exit;

            // $nombres['nombres'] = $this->session->userdata('nombres');
            $nombre['user_nombre'] = $this->session->userdata('nombre');
            $apellido['user_apellido'] = $this->session->userdata('apellidos');
            $puesto['user_puesto'] = $this->session->userdata('puesto');
            $lista = array(
                'nombre' => $nombre['user_nombre'],
                'apellido' => $apellido['user_apellido'],
                'puesto' => $puesto['user_puesto'],
            );

            // $nom_ape['nom_ape'] =$nombre['user_nombre'] ." ". $apellido['user_apellido'] ;
            // var_dump($lista['puesto']);exit;
            $this->load->view('templates/header_user', $lista);
            $this->load->view('user_View', $data);
            $this->load->view('templates/section');
            $this->load->view('templates/footer');
        } else {
            redirect(base_url());
        }
	}
	public function editar()
	{
		$this->load->view('editar_view');
	}
	public function eliminar_user()
	{

	}
    public function mostrar_user_logueado()
    {

    }
    public function cerrar_sesion()
    {
        $usuario_data = array(
            'logueado' => false,
        );
        $this->session->set_userdata($usuario_data);
        redirect(base_url());
    }

    public function register()
    {

        $this->load->model('Login_Model');
        $this->Login_Model->registrar_usuario();
        redirect(base_url() . 'user');

    }

    // public function tabla_de_usuarios()
    // {
    //     $this->load->model('Login_Model');
    //         $usuario2 = $this->Login_Model->mostrar_usuarios();
    //         u$usuario_data = array(
    //             'suario'=>$usuario->usuario,
    //             'contraseña'=>$contraseña->contraseña,
    //             'nombres' => $nombres->nombres,
    //             'apellidos' => $apellidos->apellidos,
    //             'correo' => $correo->correo,
    //         );
    //         $usuario_data['usuario'];
    //         $usuario_data['contraseña'];
    //         $usuario_data['nombres'];
    //         $usuario_data['apellidos'];
    //         $usuario_data['correo'];

    //         $this->load->view('user_View',$usuario_data);
    // }
}
